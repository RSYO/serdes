module serdes_tb ();

parameter b = 8;

reg clk;
reg rstn;

reg [b-1:0] parallel_data;
reg         send;

logic p2s_busy;
logic s2p_busy;

logic s2p_ready;
logic [b-1:0] s2p_parallel_data;


serdes DUT
(
.clk(clk),
.rstn(rstn),

.parallel_data(parallel_data),
.send(send),

.p2s_busy(p2s_busy),
.s2p_busy(s2p_busy),

.s2p_ready(s2p_ready),
.s2p_parallel_data(s2p_parallel_data)
);

always 
begin
	#5 clk = ~clk;
end

initial begin
	clk = 0;
	rstn = 1;
	#10;
	rstn = 0;
	#10;
	rstn = 1;
	parallel_data = 13;
   send = 1;
	#1000;
	$stop;
end


endmodule
