module serdes #(parameter b=8)
(
input clk,
input rstn,

input [b-1:0] parallel_data,
input         send,

output p2s_busy,
output s2p_busy,

output s2p_ready,
output [b-1:0] s2p_parallel_data
);

logic link;
logic link_req;

p2s #(.b(b)) PARALLEL_TO_SERIAL 
(
	.clk(clk),											// Master system clock
   .rstn(rstn),										// Master reset (active low)

   // Parallel data interface:
   .parallel_link(parallel_data), 				// Parallel input [b]
   .send(send), 										// Request send
	.channel_busy(s2p_busy),						// Channel is busy
   .busy(p2s_busy),									// Interface is busy

   // Serial data interface:
   .channel(link_req),								// Channel select
   .serial_link(link)								// Serial data LINK
);

// LINK DELAY


s2p #(.b(b)) SERIAL_TO_PARALLEL
(
	.clk(clk),											// Master system clock
   .rstn(rstn),										// Master reset (active low)

   // Serial data interface:
   .serial_link(link),								// Serial data LINK
   .recv(link_req),									// Channel Request
   .busy(s2p_busy),									// Interface is busy
	
   // Parallel data interface:
   .ready(s2p_ready),								// Channel ready
   .parallel_link(s2p_parallel_data)			// Parallel data [b]
);

endmodule 