module p2s #(parameter b=8)
(
	input clk,    // master system clock
   input rstn,  // master reset (active low)

   // Parallel data interface:
   input [b-1:0] parallel_link,
   input         send,
	input channel_busy,
   output logic    busy,

   // Serial data interface:
   output logic channel,     // channel select
   output logic serial_link  // serial data
);

  reg [$clog2(b)-1 : 0] bit_index;

  enum int unsigned {IDLE = 0, WRITE = 2} state, next_state;
  
	always_comb 
	begin : next_state_logic
		next_state = IDLE;
		case(state)
			IDLE: if (send) next_state = WRITE; else next_state = IDLE;
			WRITE: if (bit_index == b-1) next_state = IDLE; else next_state = WRITE;
		endcase
	end
	
	always_comb 
	begin: data_logic_asyn
		case(state)
			IDLE: 
			begin
				busy  = 0;
				channel    = 0;
				serial_link = 0;
			end
			WRITE:
			begin
				busy  = 1;
				channel    = 1;
				serial_link	=	parallel_link[bit_index];
			end
		endcase
	end

	always_ff@(posedge clk)
	begin: data_logic_sync
		case(state)
			IDLE: bit_index <= 0;
			WRITE: if (channel_busy) bit_index <= bit_index + 1; else bit_index <= 0;
			
		endcase
	end

	always_ff@(posedge clk or negedge rstn) 
	begin: state_logic_sync
		if(~rstn)
			state <= IDLE;
		else
			state <= next_state;
	end
  
endmodule






