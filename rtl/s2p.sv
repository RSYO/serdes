module s2p #(parameter b=8)
(
	input clk,    // master system clock
   input rstn,  // master reset (active low)

   // Serial data interface:
   input serial_link,
   input	recv,
   output logic busy,
	
   // Parallel data interface:
   output logic ready,     // channel select
   output logic [b-1:0] parallel_link  // serial data
);

  reg [$clog2(b)-1 : 0] bit_index;

  enum int unsigned {IDLE = 0, RECV = 2, FINISH = 4} state, next_state;
  
	always_comb 
	begin : next_state_logic
		next_state = IDLE;
		case(state)
			IDLE: if (recv) next_state = RECV; else next_state = IDLE;
			RECV: if (bit_index == b-1) next_state = FINISH; else next_state = RECV;
			FINISH: next_state = IDLE;
		endcase
	end
	
	always_comb 
	begin: data_logic_asyn
		case(state)
			IDLE: 
			begin
				busy  = 0;
				ready = 0;
			end
			RECV:
			begin
				busy  = 1;
				ready = 0;
			end
			FINISH:
			begin
				busy  = 0;
				ready = 1;

			end
		endcase
	end
	

	always_ff@(posedge clk)
	begin: data_logic_sync
		case(state)
			IDLE: 
			begin
				bit_index = 0;
				parallel_link = 0;
			end
			RECV: 
			begin
				bit_index = bit_index + 1;
				// parallel_link = {parallel_link[b-2:0],serial_link};
				parallel_link = {serial_link,parallel_link[b-1:1]};
			end
			FINISH: 
			begin
				bit_index = 0;
				parallel_link = parallel_link;
			end
		endcase
	end

	always_ff@(posedge clk or negedge rstn) 
	begin: state_logic_sync
		if(~rstn)
			state <= IDLE;
		else
			state <= next_state;
	end
  
endmodule